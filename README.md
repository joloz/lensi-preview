# Lenis Preview
A simple one-file editor to preview the basic color codes for [Lensmoor](https://lensmoor.org).

[Click here to use the online version](https://joloz.gitlab.io/lensi-preview/)

### License
This project is available under the Apache 2.0 license.
